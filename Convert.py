import requests
import os
import gzip
import binascii

input_dir = "html"              # Sub folder of webfiles
output_dir = "out"

#f_output = open("output.cpp", "w")
URL_minify_js   = 'https://javascript-minifier.com/raw' # Website to minify javascript
URL_minify_html = 'https://html-minifier.com/raw'        # Website to minify html
URL_minify_css  = 'https://cssminifier.com/raw'         # Website to minify css

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

def write_to_file(file, data, dir, file_size):
    filename, file_extension = os.path.splitext(file)       # Split filename and file extension
    file_extension = file_extension.replace(".","")         # Remove puncuation in file extension
    #file_size = os.path.getsize(dir)
    #file_size = len(data)

    f_output = open(output_dir + "/" + file + ".gz", "wb")
    f_output.write(data)
    f_output.close()

    #dir = dir.replace(input_dir,"")                         # Remove the first directory(input_dir)
    #dir = dir.replace("\\","/")                             # Chang to /
    #f_output.write("// " + dir + "\n")                      # Print comment
    #f_output.write("#define data_" + filename + "_" + file_extension + "_len " + str(file_size) + "\n")    # print size
    #f_output.write("const uint8_t data_"+filename+"_"+file_extension+"[] PROGMEM = {"+data.upper()+"};\n\n")            # print binary data

    # f_output.write("#define data_" + filename + "_len " + str(data.count('0x')) +"\n")

def aschii2Hex(text):
    output_str = ""
    x = 1
    strLen = len(text)
    for character in text:
        output_str += hex(ord(character))

        if (x != strLen):
            output_str += ","
        x += 1
    return output_str
    
def bytes2Hex(bytes):
    output_str = ""
    x = 1
    bLen = len(bytes)
    for b in bytes:
        output_str += hex(b)

        if (x != bLen):
            output_str += ","
        x += 1
    return output_str    

def minify_js(input_file):
    url = URL_minify_js
    data = {'input': open(input_file, 'rb').read()}
    response = requests.post(url, data=data)
    return response.text

def minify_html(input_file):
    url = URL_minify_html
    data = {'input': open(input_file, 'rb').read()}
    response = requests.post(url, data=data)
    return response.text

def minify_css(input_file):
    url = URL_minify_css
    data = {'input': open(input_file, 'rb').read()}
    response = requests.post(url, data=data)
    return response.text


for root, dirs, files in os.walk(input_dir, topdown=False):
    for name in files:   # for files
        if name.endswith(".js"):
            print(os.path.join(root, name))
            minified = minify_js(os.path.join(root, name))          # minify javascript
            compressed = gzip.compress(bytes(minified,'utf-8'))
            write_to_file(name, compressed, os.path.join(root, name), len(compressed)) # write to file

        elif name.endswith(".html"):
            print(os.path.join(root, name))
            minified = minify_html(os.path.join(root, name))        # minify html
            compressed = gzip.compress(bytes(minified,'utf-8'))
            write_to_file(name, compressed, os.path.join(root, name), len(compressed)) # write to file

        elif name.endswith(".css"):
            print(os.path.join(root, name))
            minified = minify_css(os.path.join(root, name))         # minify css
            compressed = gzip.compress(bytes(minified,'utf-8'))
            write_to_file(name, compressed, os.path.join(root, name), len(compressed)) # write to file
            
        else:
            print(os.path.join(root, name))
            data = open(os.path.join(root, name), 'rb').read()
            compressed = gzip.compress(data)
            write_to_file(name, compressed, os.path.join(root, name), len(compressed)) # write to file

#f_output.close()