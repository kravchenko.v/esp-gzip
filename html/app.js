"use strict";

window.tAppState = Object.freeze({
	STOP: 0,
	CONNECTING: 1,
	CONNECTED: 2,
	READY: 3
});

/*
	XX(APP_DEBUG,			0,	"Debug message") \
	XX(APP_PING,			1,	"Ping") \
	XX(APP_HEAP,			2,	"App info") \
	XX(APP_DATE_GET,		3,	"Get datetime") \
	XX(APP_DATE_SET,		4,	"Set datetime") \
	XX(APP_RESTART,			5,	"Application restart (sec)") \
	\
	XX(CNF_AP_GET,			6,	"Get ap config (password, channel)") \
	XX(CNF_AP_SET,	 		7,	"Set ap config (password, channel)") \
	\
	XX(CNF_STA_GET,			8,	"Get station config (enabled, ssid, password)") \
	XX(CNF_STA_SET,			9,	"Set station config (enabled, ssid, password)") \
	\
	XX(NET_AP_GET,			10,	"Get network ap") \
	XX(NET_STA_GET,			11,	"Get network station") \
	\
	XX(MESH_NODE_COUNT,		12,	"Get node count") \
	XX(MESH_NODE_LIST,		13,	"Get node list") \
	XX(MESH_NODE_REMOVE,	14,	"Remove node") \
	\
	XX(MESH_CHILD_GET_VALUE,15,	"Get child value") \
	XX(MESH_CHILD_REQ_VALUE,16,	"Req child value") \
	XX(MESH_CHILD_SET_VALUE,17,	"Set child value")
*/

window.tMessageType = Object.freeze({
	WST_APP_DEBUG: 0,
	WST_APP_PING: 1,
	WST_APP_INFO: 2,

	MESH_NODE_LIST: 13,

	MESH_CHILD_GET_VALUE: 15,
	MESH_CHILD_SET_VALUE: 17
	/*WST_NODES_INFO: 3,
	WST_CHILD_SEND: 4,
	WST_CHILD_VALUE: 5,
	WST_DATE_SET: 6,
	WST_RESET: 7,
	WST_REBOOT: 8,
	WST_COMMAND: 9,
	WST_REBOOT_NODE: 10,
	WST_RESET_STAT: 11,
	WST_CONFIG_SAVE: 12,
	WST_REMOVE_NODE: 13,
	WST_STA_INFO: 14,
	WST_CHILD_NAME: 15,
	WST_CONFIG_READ: 16*/
});

class Logger {
	constructor() {
		this.log("()");
	}
	log(message, name = this.constructor.name) {
		console.log(name + '->' + message);
	}
	warn(message, name = this.constructor.name) {
		console.warn(name + '->' + message);
	}
	error(message, name = this.constructor.name) {
		console.error(name + '->' + message);
	}        
}

class App extends Logger {
	constructor(hostWeb, hostWs) {
		super();
		this.hostWeb = hostWeb;
		this.hostWs = hostWs;
		this.state = tAppState.STOP;
		this.packetId = 0;
		this.server = null;
		this.config = null;
		this.nodes = null;
		this.sta = null;
		this.mm = [];

		let self = this;
		window.addEventListener("hashchange", function(e){ self.urlChanged(); });
		this.timerMinute = setInterval(function () { self.onTimerSecond(); }, 1000);
		this.timerProcess = setInterval(function() { self.mmProcess();}, 100);
	}
	onTimerSecond() {
		this.updateNodesStatus();
	}
	urlChanged() {
		this.log('urlChanged(' + this.getPage() + ')');
		let e = this.generateEvent("appState");
		e.appState = this.state;
		document.dispatchEvent(e);
	}
	getPage() {
		let s = window.location.hash.toLowerCase().replace('#','');
		let u = s.toLowerCase();
		let i = u.indexOf('#');
		if (i !== -1) u = u.substr(i + 1);
		i = u.indexOf('?');
		if (i !== -1) u = u.substr(0, i);
		return u.length === 0 ? 'home' : u;
	}
	getParamInt(param) {
		return parseInt(this.getParam(param));
	}
	getParam(param) {
		let name = param.replace(/[\[\]]/g, '\\$&');
		let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(window.location.href);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}
	start() {
		this.log("start()");
		let self = this;
		if (this.ws) {
			this.ws.refresh();
		} else {
			this.ws = new ReconnectingWebSocket(this.hostWs);
			this.ws.onopen = function (event) {
				self.onOpen();
			};
			this.ws.onclose = function (event) {
				self.onClose();
			};
			this.ws.onmessage = function (event) {
				self.onMessage(event.data);
			}
		}
		this.setState(tAppState.CONNECTING);
	}
	setState(state) {
		this.state = state;

		if (this.state === tAppState.CONNECTED) {
			this.server = null;
			this.config = null;
			this.nodes = null;
			this.sendMessage(tMessageType.WST_APP_INFO, null, function(ok) {
				app.warn(ok + ' WST_APP_INFO');
			});
			this.sendMessage(tMessageType.MESH_NODE_LIST, null, function(ok) {
				app.warn(ok + ' MESH_NODE_LIST');
			});
			/*this.sendMessage(tMessageType.WST_STA_INFO, null, function(ok) {
				app.warn(ok + ' WST_STA_INFO');
			});
			this.loadConfig();*/
		}
		else if (this.state === tAppState.READY) {
			this.fetchNodes();
		}

		this.log("state=" + this.state);
		let e = this.generateEvent("appState");
		e.appState = this.state;
		document.dispatchEvent(e);
	}
	fetchChildNames() {
		let n = 0;
		let self = this;
		for (let i = 0; i < this.nodes.length; i++) {
			for (let j = 0; j < this.nodes[i].childs.length; j++) {
				let child = self.nodes[i].childs[j];
				//TODO//self.sendMessage(tMessageType.WST_CHILD_NAME, self.int2s(child.nodeId, 3) + self.int2s(child.childId, 3));
				n++;
			}
		}
	}
	fetchNodes() {
		let n = 0;
		let self = this;
		for (let i = 0; i < this.nodes.length; i++) {
			for (let j = 0; j < this.nodes[i].childs.length; j++) {
				let child = self.nodes[i].childs[j];
				self.sendMessage(tMessageType.MESH_CHILD_GET_VALUE, child.nodeId + '|' + child.childId);
				n++;
			}
		}
	}
	sendChildVal(nodeId, childId, value) {
		this.sendMessage(tMessageType.MESH_CHILD_SET_VALUE, nodeId + '|' + childId + '|' + value);
	}
	onOpen(data) {
		this.log("onOpen");
		this.setState(tAppState.CONNECTED);
	}
	onClose(data) {
		this.log("onClose");
		this.setState(tAppState.CONNECTING);
	}
	isReady() {
		return this.state === tAppState.READY;
	}
	isLoaded() {
		return this.server != null && this.nodes != null;// this.config != null && this.server != null && this.nodes != null && this.sta != null;
	}
	checkForAllDataFetched() {
		if (this.state !== tAppState.READY && this.isLoaded()) {
			this.setState(tAppState.READY);
			return true;
		}
		return false;
	}
	onMessage(message) {
		let arr = message.split('|');
		if (arr.length < 3) return;
		let id = parseInt(arr[0]);
		let isResponse = parseInt(arr[1]) == 1;
		let type = parseInt(arr[2]);
		let args = arr.slice(3);

		switch (type) {
			case tMessageType.WST_APP_DEBUG:
				this.log(args, 'Debug');
				break;
			case tMessageType.WST_APP_PING:
				this.warn('PING');
				break;
			case tMessageType.WST_APP_INFO:
				{
					this.config = {
						configStaEnabled: false,
						configStaSSID: "",
						configStaPass: ""
					};

					this.server = {
						serverVersion: parseInt(args[0]),
						serverId: parseInt(args[1]),
						serverHeapMax: parseInt(args[2]),
						serverHeapFree: parseInt(args[3]),
						serverMillis: parseInt(args[4]),
						nodeCount: parseInt(args[5]),
						childCount: parseInt(args[6]),
						serverDate: new Date(parseInt(args[7]), parseInt(args[8]) - 1, parseInt(args[9]), parseInt(args[10]), parseInt(args[11]), parseInt(args[12])),
						name: args[13]
					};
					this.checkForAllDataFetched();
					if (this.state === tAppState.READY) document.dispatchEvent(this.generateEvent('serverInfo'));
				}
				break;
			case tMessageType.MESH_NODE_LIST:
				{
					let nodeCount = parseInt(args[0]);
					let i = 1;
					this.nodes = [];

					for (let n = 0; n < nodeCount; n++) {
						let nodeId = parseInt(args[i]);
						let deviceId = args[i+1];
						let childCount = parseInt(args[i+2]);

						let node = {};
						node.online = undefined;
						node.onlineAge = -1;
						node.nodeId = nodeId;
						node.deviceId = deviceId;
						node.name = deviceId;
						node.index = undefined;
						node.childs = [];
						this.nodes.push(node);
						let nodeIndex = this.getNodeIndexById(nodeId);
						this.nodes[nodeIndex].index = nodeIndex;
						i += 3;

						for (let j = 0; j < childCount; j++) {
							let child = {};
							child.nodeId = nodeId;
							child.childId = parseInt(args[i]);
							child.typeId = parseInt(args[i+1]);
							child.name = 'child #' + child.childId;
							child.valueTime = null;
							child.value = child.valueOld = undefined;
							child.visible = true;
							this.nodes[nodeIndex].childs.push(child);
							i += 2;
						}
					}

					this.fetchChildNames();
					this.checkForAllDataFetched();
					if (this.state === tAppState.READY) document.dispatchEvent(this.generateEvent('nodesInfo'));
				}
				break;
			case tMessageType.MESH_CHILD_GET_VALUE:
				{
					let nodeId = parseInt(args[0]);
					let childId = parseInt(args[1]);
					let age = parseInt(args[2]);
					let value = args[3];
					let valueTime = new Date((new Date()).getTime() - age * 1000);

					let nodeIndex = this.getNodeIndexById(nodeId);
					if (nodeIndex !== -1) {
						let childIndex = this.getChildIndexById(nodeId, childId);
						if (childIndex !== -1) {
							this.nodes[nodeIndex].childs[childIndex].valueTime = valueTime;
							this.nodes[nodeIndex].childs[childIndex].valueOld = this.nodes[nodeIndex].childs[childIndex].value;
							this.nodes[nodeIndex].childs[childIndex].value = value;
							this.warn(nodeId + '-' + childId + '=' + value);
							let e = this.generateEvent('childValue');
							e.nodeId = nodeId;
							e.childId = childId;
							e.valueTime = valueTime;
							e.value = value;
							this.updateNodeStatus(this.nodes[nodeIndex]);
							document.dispatchEvent(e);
						}
					}
				}
				break;
			/*case tMessageType.WST_CONFIG_READ:
				{
					let arr = data.split('|');
					this.config = {
						configStaEnabled: parseInt(arr[0]) === 1,
						configStaSSID: arr[1],
						configStaPass: arr[2]
					};
					this.checkForAllDataFetched();
					if (this.state === tAppState.READY) document.dispatchEvent(this.generateEvent('configInfo'));
				}
				break;
			case tMessageType.WST_STA_INFO:
				{
					let arr = data.split('|');
					this.sta = {
						staStatus: parseInt(arr[0]),
						staConnected: parseInt(arr[1]) === 1,
						staIp: arr[2],
						staGateway: arr[3]
					};
					this.checkForAllDataFetched();
					if (this.state === tAppState.READY) document.dispatchEvent(this.generateEvent('staInfo'));
				}
				break;*/
			case tMessageType.WST_CHILD_NAME:
				{
					let nodeId = parseInt(data.substr(0, 3));
					let childId = parseInt(data.substr(3, 3));
					let value = data.substr(6);
					let nodeIndex = this.getNodeIndexById(nodeId);
					if (nodeIndex !== -1) {
						let childIndex = this.getChildIndexById(nodeId, childId);
						if (childIndex !== -1) {
							this.nodes[nodeIndex].childs[childIndex].name = value;
							let e = self.app.generateEvent('childName');
							e.nodeId = nodeId;
							e.childId = childId;
							e.value = value;
							document.dispatchEvent(e);                            
						}
					}                    
				}
				break;
		}

		for (let i = 0; i < this.mm.length; i++) {
			if (this.mm[i].packetId == id) {
				this.mm[i].callback(true);
				this.mm.splice(i, 1);
			}
		}
	}
	isNodeChildsLoaded(nodeId) {
		let nodeIndex = this.getNodeIndexById(nodeId);
		if (nodeIndex === -1) return false;
		for (let i = 0; i < this.nodes[nodeIndex].childs.length; i++) {
			if (this.nodes[nodeIndex].childs[i].name == null) {
				return false;
			}
		}
		return true;
	}
	getNodeById(nodeId) {
		for (let i = 0; this.nodes != null && i < this.nodes.length; i++) {
			if (this.nodes[i].nodeId === nodeId)
				return this.nodes[i];
		}
		return null;
	}
	getNodeIndexById(nodeId) {
		for (let i = 0; this.nodes != null && i < this.nodes.length; i++) {
			if (this.nodes[i].nodeId === nodeId)
				return i;
		}
		return -1;
	}
	getChildById(nodeId, childId) {
		let nodeIndex = this.getNodeIndexById(nodeId);
		if (nodeIndex === -1) return null;
		for (let i = 0; i < this.nodes[nodeIndex].childs.length; i++) {
			if (this.nodes[nodeIndex].childs[i].childId === childId)
				return this.nodes[nodeIndex].childs[i];
		}
		return null;
	}
	getChildIndexById(nodeId, childId) {
		let nodeIndex = this.getNodeIndexById(nodeId);
		if (nodeIndex === -1) return -1;
		for (let i = 0; i < this.nodes[nodeIndex].childs.length; i++) {
			if (this.nodes[nodeIndex].childs[i].childId === childId)
				return i;
		}
		return -1;
	}
	mmProcess() {

		if (this.ws) {
			let ind = -1;
			for (let i = 0; i < this.mm.length; i++) {
				if (this.mm[i].new) {
					ind = i;
					this.mm[i].new = false;
					this.mm[i].tsend = new Date();
					this.mm[i].rn++;
					break;
				}
			}
			if (ind !== -1) {
				this.log(' send(' + this.mm[ind].type + ',' + (this.mm[ind].data == null ? 'null' : this.mm[ind].data) + ')');
				//this.ws.send(this.int2s(this.mm[ind].packetId, 3) + this.int2s(this.mm[ind].type, 3) + (this.mm[ind].data == null ? '' : this.mm[ind].data));
				this.ws.send(this.mm[ind].packetId + '|0|' + this.mm[ind].type + (this.mm[ind].data == null ? '' : '|' + this.mm[ind].data));
				if (this.mm[ind].callback === null) {
					this.mm.splice(ind, 1);
				}
			}
		}

		for (let i = 0; i < this.mm.length; i++) {
			if (!this.mm[i].new) {
				let tdiff = new Date() - this.mm[i].tsend;
				if (tdiff >= 500) {
					if (this.mm[i].rn >= this.mm[i].retry) {
						this.mm[i].callback(false);
						this.mm.splice(i, 1);
					} else {
						this.mm[i].new = true;
						this.mm[i].tsend = null;
					}
				}
			}
		}
	}
	sendMessage(type, data = null, callback = null, retry = 1) {
		this.packetId = (this.packetId === 256 ? 1 : this.packetId + 1);

		let m = {};
		m.type = type;
		m.packetId = this.packetId;
		m.data = data;
		m.callback = callback;
		m.new = true;
		m.tadd = new Date();
		m.tsend = null;
		m.rn = 0;
		m.retry = retry;
		this.mm.push(m);

		this.log(' push(' + type + ',' + (data == null ? 'null' : data) + ')');
		return this.packetId;
	}
	loadConfig() {
		this.sendMessage(tMessageType.WST_CONFIG_READ, null, function(ok) {
			app.warn(ok + ' WST_CONFIG_READ');
		});
	}
	generateEvent(s) {
		let evt = document.createEvent("CustomEvent");
		evt.initCustomEvent(s, false, false, null);
		return evt;
	}
	int2s(value, n) {
		return ('0'.repeat(n) + value).slice(-n);
	}
	str2s(value, n) {
		return value.length > n ? value.substr(0, n) : (value + '\0'.repeat(n - value.length));
	}
	ajax(url, func) {
		let request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.onload = function () {
			if (this.status >= 200 && this.status < 400) {
				func(true, this.response);
			}
			else {
				func(false);
			}
		};
		request.onerror = function () {
			func(false);
		};
		request.send();
	}
	saveConfig(cStaEnabled, cStaSSID, cStaPass) {
		if (cStaEnabled) {
			this.sendMessage(tMessageType.WST_CONFIG_SAVE,this.int2s(cStaEnabled.checked ? 1 : 0, 3) + this.str2s(cStaSSID.value, 32) + this.str2s(cStaPass.value, 32));
			this.loadConfig();
			this.goHome();
		}
	}
	saveDate(cDate) {
		let value = cDate.value;

		if (value.length == 19) {
			this.sendMessage(tMessageType.WST_DATE_SET, 
				this.int2s(value.substr(6, 4), 3) + 
				this.int2s(value.substr(3, 2), 3) + 
				this.int2s(value.substr(0, 2), 3) + 
				this.int2s(value.substr(11, 2), 3) + 
				this.int2s(value.substr(14, 2), 3) + 
				this.int2s(value.substr(17, 2), 3)
			);
			this.sendMessage(tMessageType.WST_SERVER_INFO);
			this.goHome();
		}
	}
	goHome() {
		this.goto('home', true);
	}
	goto(page, reload = false) {
		this.log('goto(' + page + ')');
		window.history.pushState(null, null, '#' + page);
		if (reload) location.reload();
		else {
			location.href = '#' + page;
			this.urlChanged();
		}
	}
	restartNode(node) {
		this.sendMessage(tMessageType.WST_REBOOT_NODE, this.int2s(node.nodeId, 3));
		let nodeIndex = this.getNodeIndexById(node.nodeId);
		if (nodeIndex === -1) return;
		for (let i = 0; i < this.nodes[nodeIndex].childs.length; i++) {
			this.nodes[nodeIndex].childs[i].valueTime = null;
		}
		this.goto('node?id=' + node.nodeId);
		this.updateNodeStatus(node);
	}
	removeNode(node) {
		this.sendMessage(tMessageType.WST_REMOVE_NODE, this.int2s(node.nodeId, 3));
		this.goHome();
	}
	getNodeValueTime(node) {
		let lastTime = null;
		for (let i = 0; i < node.childs.length; i++) {
			let time = node.childs[i].valueTime;
			if ((!lastTime && time) || (lastTime && time && time.getTime() > lastTime.getTime())) {
				lastTime = time;
			}
		}
		return lastTime;
	}
	updateNodeStatus(node) {
		let nodeIndex = this.getNodeIndexById(node.nodeId);
		if (nodeIndex !== -1) {
			let online = false;
			let onlineAge = -1;
			let valueTime = this.getNodeValueTime(node);
			if (valueTime) {
				let nowTime = new Date();
				let diffTimeSec = Math.round((nowTime.getTime() - valueTime.getTime()) / 1000);
				onlineAge = diffTimeSec;
				if (diffTimeSec < 60) {
					online = true;
				}
			}

			if (this.nodes[nodeIndex].onlineAge !== onlineAge) {
				this.nodes[nodeIndex].onlineAge = onlineAge;
				let e = self.app.generateEvent('nodeAge');
				e.nodeId = node.nodeId;
				e.onlineAge = onlineAge;
				document.dispatchEvent(e);
			}

			if (this.nodes[nodeIndex].online !== online) {
				this.nodes[nodeIndex].online = online;
				let e = self.app.generateEvent('nodeStatus');
				e.nodeId = node.nodeId;
				e.online = online;
				document.dispatchEvent(e);
			}
		}
	}
	updateNodesStatus() {
		if (this.isReady()) {
			for (let i = 0; i < this.nodes.length; i++) {
				this.updateNodeStatus(this.nodes[i]);
			}
		}
	}
}

class ReconnectingWebSocket {
	constructor(url) {

		ReconnectingWebSocket.CONNECTING = WebSocket.CONNECTING;
		ReconnectingWebSocket.OPEN = WebSocket.OPEN;
		ReconnectingWebSocket.CLOSING = WebSocket.CLOSING;
		ReconnectingWebSocket.CLOSED = WebSocket.CLOSED;

		this.app = app;
		this.debug = false;
		this.automaticOpen = true;
		this.reconnectInterval = 1000;
		this.maxReconnectInterval = 30000;
		this.reconnectDecay = 1.5;
		this.timeoutInterval = 2000;
		this.maxReconnectAttempts = null;
		this.binaryType = 'blob';
		this.url = url;
		this.reconnectAttempts = 0;
		this.readyState = WebSocket.CONNECTING;
		this.protocol = null;

		this.forcedClose = false;
		this.timedOut = false;

		let self = this;
		document.addEventListener('open',       function(event) { self.onopen(event); });
		document.addEventListener('close',      function(event) { self.onclose(event); });
		document.addEventListener('connecting', function(event) { self.onconnecting(event); });
		document.addEventListener('message',    function(event) { self.onmessage(event); });
		document.addEventListener('error',      function(event) { self.onerror(event); });

		this.addEventListener = document.addEventListener.bind(document.body);
		this.removeEventListener = document.removeEventListener.bind(document.body);
		this.dispatchEvent = document.dispatchEvent.bind(document.body);

		if (this.automaticOpen === true) {
			this.open(false);
		}
	}
	open(reconnectAttempt) {
		let self = this;
		this.ws = new WebSocket(this.url, []);
		this.ws.binaryType = this.binaryType;

		if (reconnectAttempt) {
			if (this.maxReconnectAttempts && this.reconnectAttempts > this.maxReconnectAttempts) {
				return;
			}
		} else {
			document.dispatchEvent(this.app.generateEvent('connecting'));
			this.reconnectAttempts = 0;
		}

		if (this.debug) {
			console.debug('ReconnectingWebSocket', 'attempt-connect', this.url);
		}

		let timeout = setTimeout(function() {
			if (self.debug) {
				console.debug('ReconnectingWebSocket', 'connection-timeout', self.url);
			}
			self.timedOut = true;
			self.ws.close();
			self.timedOut = false;
		}, this.timeoutInterval);

		this.ws.onopen = function(event) {
			clearTimeout(timeout);
			if (self.debug) {
				console.debug('ReconnectingWebSocket', 'onopen', self.url);
			}
			self.protocol = self.ws.protocol;
			self.readyState = WebSocket.OPEN;
			self.reconnectAttempts = 0;
			let e = self.app.generateEvent('open');
			e.isReconnect = reconnectAttempt;
			reconnectAttempt = false;
			document.dispatchEvent(e);
		};

		this.ws.onclose = function(event) {
			clearTimeout(timeout);
			this.ws = null;
			if (this.forcedClose) {
				self.readyState = WebSocket.CLOSED;
				document.dispatchEvent(self.app.generateEvent('close'));
			} else {
				self.readyState = WebSocket.CONNECTING;
				let e = self.app.generateEvent('connecting');
				e.code = event.code;
				e.reason = event.reason;
				e.wasClean = event.wasClean;
				document.dispatchEvent(e);
				if (!reconnectAttempt && !self.timedOut) {
					if (self.debug) {
						console.debug('ReconnectingWebSocket', 'onclose', self.url);
					}
					document.dispatchEvent(self.app.generateEvent('close'));
				}

				let timeout = self.reconnectInterval * Math.pow(self.reconnectDecay, self.reconnectAttempts);
				setTimeout(function() {
					self.reconnectAttempts++;
					self.open(true);
				}, timeout > self.maxReconnectInterval ? self.maxReconnectInterval : timeout);
			}
		};
		this.ws.onmessage = function(event) {
			if (self.debug) {
				console.debug('ReconnectingWebSocket', 'onmessage', self.url, event.data);
			}
			let e = self.app.generateEvent('message');
			e.data = event.data;
			document.dispatchEvent(e);
		};
		this.ws.onerror = function(event) {
			if (self.debug) {
				console.debug('ReconnectingWebSocket', 'onerror', self.url, event);
			}
			document.dispatchEvent(self.app.generateEvent('error'));
		};
	}
	send(data) {
		if (this.ws) {
			if (this.debug) {
				console.debug('ReconnectingWebSocket', 'send', this.url, data);
			}
			return this.ws.send(data);
		} else {
			throw 'INVALID_STATE_ERR : Pausing to reconnect websocket';
		}
	}
	close(code, reason) {
		// Default CLOSE_NORMAL code
		if (typeof code == 'undefined') {
			code = 1000;
		}
		this.forcedClose = true;
		if (this.ws) {
			this.ws.close(code, reason);
		}
	}
	refresh() {
		if (this.ws) {
			this.ws.close();
		}
	}
	onopen(event) {};
	onclose(event) {};
	onconnecting(event) {};
	onmessage(event) {};
	onerror(event) {};
}